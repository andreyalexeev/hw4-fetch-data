const filmsUrl = 'https://ajax.test-danit.com/api/swapi/films';

        async function fetchFilms() {
            try {
                const response = await fetch(filmsUrl);
                const films = await response.json();

                const filmsContainer = document.getElementById('films-container');
                filmsContainer.innerHTML = '';

                for (const film of films) {
                    const filmDiv = document.createElement('div');
                    filmDiv.classList.add('film');
                    filmDiv.innerHTML = `
                        <h2>Епізод ${film.episodeId}: ${film.name}</h2>
                        <p>${film.openingCrawl}</p>
                        <p class="loading">Завантаження персонажів...</p>
                        <ul class="characters"></ul>
                    `;
                    filmsContainer.appendChild(filmDiv);

                    fetchCharacters(film.characters, filmDiv);
                }
            } catch (error) {
                console.error('Помилка при отриманні списку фільмів:', error);
            }
        }

        async function fetchCharacters(characterUrls, filmDiv) {
            try {
                const characterPromises = characterUrls.map(url => fetch(url).then(res => res.json()));
                const characters = await Promise.all(characterPromises);

                const charactersList = filmDiv.querySelector('.characters');
                charactersList.previousElementSibling.remove(); // Видаляємо "Завантаження персонажів..."
                characters.forEach(character => {
                    const listItem = document.createElement('li');
                    listItem.textContent = character.name;
                    charactersList.appendChild(listItem);
                });
            } catch (error) {
                console.error('Помилка при отриманні списку персонажів:', error);
            }
        }

        document.addEventListener('DOMContentLoaded', fetchFilms);